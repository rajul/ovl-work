#!/usr/bin/perl
use strict;
use warnings;
use Email::Valid;

# Declaring configuration variables
my $NUMBER_OF_FIELDS_IN_CSV = 5;
my @EXPECTED_FIELDS = ("email", "name", "phone", "organisation", "group");
my @REQUIRED_FIELDS = ();
my @EXPECTED_GROUPS = (); # Make an API call to RT to get this list

my $file = $ARGV[0] or die "Need to get CSV file on the command line\n";
 
open(my $data, '<', $file) or die "Could not open '$file' $!\n";
 
while (my $line = <$data>) {
  chomp $line;

  # need to look into embedded commas
  my @fields = split "," , $line;	# should possibly declare @fields as global

  # validate_number_of_fields
  # validate_required_fields
  # validate_email_address
  # validate_real_name
  # validate_phone_number
  # validate_organisation
  # validate_group

  my $num_fields_validation_result = validate_number_of_fields(@fields);
  
  my %hash_of_fields = create_hash_of_fields(@fields);

  my $required_field_validation_result = validate_required_fields();

  my $email_address = $hash_of_fields{"email"};
  my $email_validation_result = validate_email_address($email_address);

  my $real_name = $hash_of_fields{"name"};
  my $real_name_validation_result = validate_real_name($real_name);

  my $phone_number = $hash_of_fields{"phone"};
  my $phone_number_validation_result = validate_phone_number($phone_number);

  my $organisation = $hash_of_fields{"organisation"};
  my $organisation_validation_result = validate_organisation($organisation);

  my $group = $hash_of_fields{"group"};
  my $group_validation_result = validate_group($group);

}

sub validate_number_of_fields {
	my @fields = @_;
	my $len = @fields;
		
	# Validating length of array
	if($len != $NUMBER_OF_FIELDS_IN_CSV) {
		return 1;	# Invalid number of fields
	}

	return 0;	# Valid number of fields
}

sub create_hash_of_fields {
	my @fields = @_;
	my %hash_of_fields;

	for(my $i=0; $i < $NUMBER_OF_FIELDS_IN_CSV; $i++) {
		my $key = $EXPECTED_FIELDS[$i];
		my $value = $fields[$i];

		# print $key;
		# print $value;
		$hash_of_fields{$key} = $value;
	}

	return %hash_of_fields;
}

sub validate_required_fields {
	return 1;
}

sub validate_email_address {
	my ($email) = @_;
	my $is_address_valid = Email::Valid->address($email);

	my $return_value = ($is_address_valid ? 0 : 1);

	return $return_value;
}

sub validate_real_name {
	my ($name) = @_;

    if ($name =~ /^[a-zA-Z]+$/){
        return 0;	# Valid name
    }

	return 1;	# Invalid name
}

sub validate_phone_number {
	return 1;
}

sub validate_organisation {
	return 1;
}

sub validate_group {
	return 1;
}