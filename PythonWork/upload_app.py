from flask import Flask, render_template, request
from werkzeug import secure_filename

from validator import OvlRTApprovalTicketCreator

app = Flask(__name__)

@app.route('/upload')
def upload_file():
	return '''
	<html>
	<body>
      <form action = "http://localhost:5000/uploader" method = "POST" 
         enctype = "multipart/form-data">
         <input type = "file" name = "file" />
         <input type = "submit"/>
      </form>
    </body>
    </html>
    '''
    # return render_template('upload.html')
	
@app.route('/uploader', methods = ['POST'])
def upload_file_handler():
   if request.method == 'POST':
      f = request.files['file']
      f.save(secure_filename(f.filename))

      ticket_creator = OvlRTApprovalTicketCreator('test2.csv')
      ticket_creator.create_tickets()

      return 'file uploaded successfully'
		
if __name__ == '__main__':
   app.run(debug = True)