#!/usr/bin/python
import re

from rtkit.resource import RTResource
from rtkit.authenticators import BasicAuthenticator
from rtkit.errors import RTResourceError

from rtkit import set_logging
import logging

import requests,logging
import json



set_logging('debug')
logger = logging.getLogger('rtkit')

class OvlUserCSVRecordValidator:
	NUMBER_OF_FIELDS_IN_CSV = 5
	EXPECTED_FIELDS = ("email", "name", "phone", "organisation", "group")
	EMAIL_REGEX = re.compile(r'[^@]+@[^@]+\.[^@]+')
	NAME_REGEX = re.compile(r'^[a-zA-Z]+$')

	def __init__(self):
		self.csv_data = []
		self.data = {}

	def validate_record_number_of_fields(self):
		return len(self.csv_data) == self.NUMBER_OF_FIELDS_IN_CSV

	def create_data_map(self):
		for i in range(self.NUMBER_OF_FIELDS_IN_CSV):
			self.data[self.EXPECTED_FIELDS[i]] = data[i] 

	def validate_required_fields(self):
		return True

	def validate_record_email_address(self):
		if not self.EMAIL_REGEX.match(self.data['email']):
			return False

		return True

	def validate_real_name(self):
		if not self.NAME_REGEX.match(self.data['name']):
			return False

		return True

	def validate_phone_number(self):
		return True

	def validate_organisation(self):
		return True

	def validate_group(self):
		return True

	def validate_record(self, csv_line):
		self.data = csv_line.strip().split(',')

		if self.validate_record_number_of_fields():
			self.create_data_map()
		else:
			return False

		if not self.validate_record_email_address():
			return False

		if not self.validate_real_name():
			return False

		if not self.validate_phone_number():
			return False

		if not self.validate_group():
			return False

		if not self.validate_organisation():
			return False

		return True


class OvlUserCSVFileValidator:
	GOOD_RECORDS_FILEPATH = 'good.csv'
	BAD_RECORDS_FILEPATH = 'bad.csv'
		
	def __init__(self, filepath, good_records_filepath=None, bad_records_filepath=None):
		self.file = open(filepath, 'r')

		if good_records_filepath:
			self.GOOD_RECORDS_FILEPATH = good_records_filepath

		if bad_records_filepath:
			self.BAD_RECORDS_FILEPATH = bad_records_filepath

		self.good_records_file = open(self.GOOD_RECORDS_FILEPATH, 'w')
		self.bad_records_file = open(self.BAD_RECORDS_FILEPATH, 'w')

		self.good_records_count = 0
		self.bad_records_count = 0

		self.record_validator = OvlUserCSVRecordValidator()

	def validate_file(self):
		for line in self.file:
			if self.record_validator.validate_record(line):
				self.good_records_file.write(line)
				self.good_records_count = self.good_records_count + 1
			else:
				self.bad_records_file.write(line)
				self.bad_records_count = self.bad_records_count + 1

		return True

class OvlRTApprovalTicketCreator:
	RT_HOST = 'localhost:8080'
	USER = 'root'
	PASSWORD = 'password'

	APPROVALS_QUEUE = 'UserCreationApprovalTest'
	EXCEPTIONS_QUEUE = 'UserCreationExceptionTest'

	APPROVALS_SUBJECT = 'DummySubjectApproval'
	EXCEPTIONS_SUBJECT = 'DummySubjectApproval'

	APPROVALS_TEXT = 'DummyTextApproval'
	EXCEPTIONS_TEXT = 'DummyTextApproval'

	def __init__(self, uploaded_filepath):
		self.file_validator = OvlUserCSVFileValidator(uploaded_filepath)
		self.file_validator.validate_file()

		self.rt_url = self.get_rt_url_string()
		self.ticket_creation_url = self.get_rt_ticket_creation_url_string('ticket/new')

		self.rt_connection = RTResource(self.rt_url, self.USER, self.PASSWORD, BasicAuthenticator)

	def get_rt_url_string(self):
		return 'https://' + self.RT_HOST + '/REST/1.0/'


	def get_rt_ticket_creation_url_string(self, suffix):
		return self.rt_url + suffix + self.get_user_string()

	def get_user_string(self):
		return '?user=' + self.USER + '&pass=' + self.PASSWORD

	def comment_with_attachment(self, ticket_id, attachment_file):
		try:
		    params = {
		        'content': {
		            'Action': 'comment',
		            'Text': 'Comment with attach',
		            'Attachment': 'x.csv',
		        },
		        'attachment_1': file(attachment_file)
		    }

		    comment_path = 'ticket/' + str(ticket_id) + '/comment'

		    response = resource.post(path=comment_path, payload=params,)
		    for r in response.parsed:
		            logger.info(r)
		except RTResourceError as e:
		    logger.error(e.response.status_int)
		    logger.error(e.response.status)
		    logger.error(e.response.parsed)


	def create_good_record_ticket(self):
		content = {
			'content': {
				'Queue': self.APPROVALS_QUEUE,
				'Subject': self.APPROVALS_SUBJECT,
				'Text': self.APPROVALS_TEXT
			}
		}

		# post_data = r'''
		# Queue: %s
		# Subject: %s
		# Text: %s
		# ''' %(self.APPROVALS_QUEUE, self.APPROVALS_SUBJECT, self.APPROVALS_TEXT)

		# print post_data
		# print self.ticket_creation_url

		# payload = {'user': self.USER, 'pass': self.PASSWORD,'content':post_data}
		# print payload
		try:
			# response = requests.post(self.ticket_creation_url, payload)
			response = self.rt_connection.post(path='ticket/new', payload=content,)		
			# logger.info(response.text)
			logger.info(response.parsed)
			# Create comment with `good` attachment
		except Exception as e:
			# logger.error(e.text)
			logger.error(e.response.status_int)
			logger.error(e.response.status)
			logger.error(e.response.parsed)
			
		
	def create_bad_record_ticket(self):
		content = {
			'content': {
				'Queue': self.EXCEPTIONS_QUEUE,
				'Subject': self.EXCEPTIONS_SUBJECT,
				'Text': self.EXCEPTIONS_TEXT
			}
		}

		try:
			response = self.rt_connection.post(path='ticket/new' + self.get_user_string(), payload=content,)		
			logger.info(response.parsed)
			# Create comment with `bad` attachment
		except RTResourceError as e:
			print e
			logger.error(e.response.status_int)
			logger.error(e.response.status)
			logger.error(e.response.parsed)

	def create_tickets(self):
		self.create_good_record_ticket()
		self.create_bad_record_ticket()

if __name__ == '__main__':
	ticket_creator = OvlRTApprovalTicketCreator('test2.csv')
	ticket_creator.create_tickets()